\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {russian}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Задание}{2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Реферат}{3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Введение}{6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Мультимедийный фреймворк}{8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Обзор фреймворков}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}GStreamer и FFmpeg}{10}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Алгоритмы сжатия}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Алгоритмы сжатия без потерь}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.1}Фундаментальные методы сжатия}{12}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {2.1.1.1}Кодирование повторов}{13}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {2.1.1.2}Преобразование Берроуза-Уилера}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.2}Алгоритмы энтропийного кодирования}{14}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {2.1.2.1}Алгоритм Шеннона $-$ Фано}{15}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {2.1.2.2}Алгоритм Хаффмана}{17}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {2.1.2.3}Арифметическое кодирование}{19}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.3}Подстановочные алгоритмы сжатия}{20}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {2.1.3.1}LZ77}{20}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Алгоритмы сжатия с потерями}{21}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.1}Алгоритмы выделения границ}{22}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {2.2.1.1}Оператор Собеля}{22}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {2.2.1.2}Дискретный оператор Лапласа}{24}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {2.2.1.3}Детектор границ Кэнни}{25}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.2}Псевдотонирование}{28}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {2.2.2.1}Алгоритм рассеивания ошибок Флойда-Стейнберга}{30}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {2.2.2.2}Алгоритм рассеивания ошибок Штуки}{32}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {2.2.2.3}Алгоритм рассеивания ошибок Аткинсона}{32}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Реализация}{34}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Архитектура GStreamer}{34}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Существующие стандарты}{38}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.1}Стандарт H.263P}{38}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.2}Стандарт H.264}{39}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.3}Стандарт MPEG-4 Part 14}{39}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.4}Стандарт JPEG}{40}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}?????}{40}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Анализ алгоритмов сжатия с потерями}{41}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Заключение}{43}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Список использованных источников}{44}
